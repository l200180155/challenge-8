describe("AuthentificationController", () => {
    it("should be able to bark and return 'Woof!'", () => {
      const userModel = new UserModel();
      const roleModel = new RoleModel();
      const bcrypt = new Bcrypt();
      const jwt = new Jwt();

      expect(userModel.bark()).toEqual();
      expect(bcrypt.bark()).toEqual();
      expect(roleModel.bark()).toEqual();
      expect(jwt.bark()).toEqual();
    })
  })
  