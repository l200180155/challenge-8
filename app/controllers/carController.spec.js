const { Dayjs } = require("dayjs");

describe("CarController", () => {
    it("should be able to bark and return 'Woof!'", () => {
      const carModel = new CarModel();
      const userCarModel = new UserCarModel();
      const dayjs = new Dayjs();

      expect(carModel.bark()).toEqual();
      expect(userCarModel.bark()).toEqual();
      expect(dayjs.bark()).toEqual();
    })
  })
  