describe("EmailNotRegisteredError", () => {
    it("should have email called 'a@gmail.com'", () => {
      const email = new Email("a@gmail.com");
  
      expect(email).toHaveProperty("email", "a@gmail.com");
    })
  
    it("should be able to bark and return 'a@gmail.com'", () => {
      const email = new Email("a@gmail.com");
      expect(email.bark()).toEqual("a@gmail.com");
    })
  })
  