describe("NotFoundError", () => {
    it("should have method and url called 'Not found'", () => {
      const method = new Method("Not found");
      const url = new Url("not found");

      expect(method).toHaveProperty("method", "Not found");
      expect(url).toHaveProperty("url", "Not found");
    })
  
    it("should be able to bark and return 'Not found'", () => {
      const method = new Method("Not found");
      const url = new Url("Not found");

      expect(method.bark()).toEqual("Not found");
      expect(url.bark()).toEqual("Not found");
    })
  })
  