describe("CarAlreadyRentedError", () => {
    it("should have car name called 'bmw'", () => {
      const car = new Car("bmw");
  
      expect(car).toHaveProperty("name", "bmw");
    })
  
    it("should be able to bark and return 'bmw'", () => {
      const car = new Car("bmw");
      expect(car.bark()).toEqual("bmw");
    })
  })
  