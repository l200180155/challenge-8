describe("InsufficientAccessError", () => {
    it("should have rolw called 'Access forbiden'", () => {
      const role = new Role("Access forbiden");
  
      expect(role).toHaveProperty("role", "Access forbiden");
    })
  
    it("should be able to bark and return 'Access forbiden'", () => {
      const role = new Role("Access forbiden");
      expect(role.bark()).toEqual("Access forbiden");
    })
  })
  