describe("RecordNotFoundError", () => {
    it("should have name called 'Not found'", () => {
      const name = new Name("Not found");

      expect(name).toHaveProperty("name", "Not found");
    })
  })
  