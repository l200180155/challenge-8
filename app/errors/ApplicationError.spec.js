describe("ApplicationError", () => {
    it("should be able to bark and return 'Woof!'", () => {
      const name = new Name();
      const message = new Message();
      const details = new Details();

      expect(name.bark()).toEqual();
      expect(message.bark()).toEqual();
      expect(details.bark()).toEqual();
    })
  })
  